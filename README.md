# Rasterizer



## How To Get Started

Create a new Qt project at `/rasterizer` , build the project and run `rasterizer.exe` .



## Project Hierarchy

+ `rasterizer.pro`, `widget.h`, `widget.cpp`, `widget.ui` : Qt widget application program.
+ `object.h`, `object.cpp` : Basic geometry primitives.
+ `geometry.h`, `geometry.cpp` : Basic geometry operations.
+ `rasterize.h`, `rasterize.cpp` : Rasterization algorithm of 2D polygon.
+ `clip.h`, `clip.cpp` : Clipping algorithm of two polygons.



## Requirements

Windows 10 64-bits

Qt5 MinGW 32-bits