#ifndef CLIP_H
#define CLIP_H

#include <list>
#include <vector>
using namespace std;

#include "object.h"

void insertToPoints(Point& p, vector<list<Point>>&& v, Point& lineStart, Point& lineEnd);

pair<Point, unsigned> traverse(Point& start, list<Point>& inPoints, list<Point>& outPoints, vector<list<Point>>& polygonPoints, vector<bool>& visited, Polygon& polygon);

vector<Polygon>* Weiler_Atherton(Polygon& subject, Polygon& clip);

#endif // CLIP_H
