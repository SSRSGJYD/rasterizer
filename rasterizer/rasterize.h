#ifndef RASTERIZE_H
#define RASTERIZE_H

#include <queue>
#include <vector>
using namespace std;

#include "object.h"

class NETNode
{
public:
    Point bottom;
    Point top;
    bool horizontal;
    double slope;
public:
    NETNode(Point& p1, Point& p2);
    bool operator <(const NETNode& node);
};

class AETNode
{
public:
    double x; // x of cross point
    double deltaX; // delta x of two neighbor cross points on the edge
    int ymax; // y of top scan line that cross the edge
    Point bottom;
    Point top;
public:
    AETNode();
    AETNode(NETNode& node);
    bool operator <(const AETNode& node);
};

queue<pair<Point, Point> > rasterize(Polygon& p);

#endif // RASTERIZE_H
