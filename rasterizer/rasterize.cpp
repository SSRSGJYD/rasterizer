#include "rasterize.h"
#include <algorithm>
#include <list>
#include <unordered_map>
using namespace std;

NETNode::NETNode(Point& p1, Point& p2){
    horizontal = false;
    if(p1.y < p2.y - 1e-6){
        top = p2;
        bottom = p1;
        slope = (this->bottom.x-this->top.x) / (this->bottom.y-this->top.y);
    }
    else if(p1.y > p2.y + 1e-6){
        top = p1;
        bottom = p2;
        slope = (this->bottom.x-this->top.x) / (this->bottom.y-this->top.y);
    }
    else{
        horizontal = true;
        if(p1.x < p2.x){
            top = p2;
            bottom = p1;
        }
        else {
            top = p1;
            bottom = p2;
        }
    }
}

bool NETNode::operator <(const NETNode& node){
    if(this->bottom.x < node.bottom.x - 1e-6)
        return true;
    if(this->bottom.x > node.bottom.x + 1e-6)
        return false;
    // x is equal
    return this->slope < node.slope;
}

AETNode::AETNode() {}

AETNode::AETNode(NETNode& node) {
    x = node.bottom.x;
    deltaX = node.slope;
    ymax = int(node.top.y);
    bottom = node.bottom;
    top = node.top;
}

bool AETNode::operator <(const AETNode& node){
    if(this->x < node.x - 1e-6)
        return true;
    if(this->x > node.x + 1e-6)
        return false;
    // x is equal
    return this->deltaX < node.deltaX;
}

queue<pair<Point, Point> > rasterize(Polygon& p)
{
    queue<pair<Point, Point> > fillLines;
    list<AETNode> AET; // active edge table
    unordered_map<int, list<NETNode>> NET; // new edge table
    int ymin = INT_MAX;
    int ymax = INT_MIN;

    // initialize NET
    size_t num1 = p.vertices.size();
    for(size_t i=0; i<num1; i++){
        NETNode node(p.vertices[i], p.vertices[(i+1) % num1]);
        if(node.horizontal) continue;
        int y = int(node.bottom.y);
        ymin = min(ymin, y);
        ymax = max(ymax, int(node.top.y));
        auto it = NET[y].begin();
        for(; it != NET[y].end(); it++){
            if(node < *it) break;
        }
        NET[y].insert(it, node);
    }
    size_t num2 = p.innerVertices.size();
    for(size_t j=0; j<num2; j++){
        num1 = p.innerVertices[j].size();
        for(size_t i=0; i<num1; i++){
            NETNode node(p.innerVertices[j][i], p.innerVertices[j][(i+1) % num1]);
            if(node.horizontal) continue;
            int y = int(node.bottom.y);
            ymin = min(ymin, y);
            ymax = max(ymax, y);
            auto it = NET[y].begin();
            for(; it != NET[y].end(); it++){
                if(node < *it) break;
            }
            NET[y].insert(it, node);
        }
    }

    // scan line from bottom to top
    for(int y=ymin; y<=ymax; ){
        // insert edges from NET to AET
        if(NET.count(y) > 0){
            auto it = NET[y].begin();
            for(; it != NET[y].end(); it++){
                AETNode node(*it);
                // insert into AET
                auto it2 = AET.begin();
                for(; it2 != AET.end(); it2++){
                    if(node < *it2) break;
                }
                AET.insert(it2, node);
            }
        }
        // match into pairs
        auto it2 = AET.begin();
        for(; it2 != AET.end(); it2++){
            Point p1(ceil(it2->x), y);
            it2++;
            Point p2(floor(it2->x), y);
            fillLines.push(pair<Point, Point>(p1, p2));
        }
        y++;
        if(y >= ymax) break;
        // remove edges with top.y == y
        it2 = AET.begin();
        for(; it2 != AET.end(); it2++){
            while(it2->ymax == y)
                it2 = AET.erase(it2);
            // update x
            it2->x += it2->deltaX;
        }
    }

    return fillLines;
}
