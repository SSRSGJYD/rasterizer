#ifndef WIDGET_H
#define WIDGET_H

#include <QButtonGroup>
#include <QWidget>

#include "object.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
    enum {VIEW, OUTER_RING, INNER_RING, TRANSLATE, ROTATE, SCALE, CLIP};

private:
    Ui::Widget *ui;
    QButtonGroup *btnGroup;

    // window: 1000 * 700
    int windowx;
    int windowy;
    const int windoww = 500;
    const int windowh = 350;

    int mode; //OUTER_RING, INNER_RING, TRANSLATE, ROTATE, SCALE

    vector<Polygon> polygons; // all polygons painted
    Polygon *newPolygon;
    vector<Point> *newInnerRing;

    // draw outer or inner ring
    Point lastPoint;
    Point newPoint;

    // choose color
    QColor borderColor;
    QColor fillColor;

    // translate
    bool trackingMouse;
    int translateStartX;
    int translateStartY;
    int translateEndX;
    int translateEndY;

    //clip
    int subjectIndex; // idx of subject polygon combo box
    int clipIndex; // idx of clip polygon combo box
    vector<Polygon>* clipPolygons; // result of clipping

protected:
    void paintEvent(QPaintEvent *);//重写窗体重绘事件
    void mousePressEvent(QMouseEvent *);//重写鼠标按下事件
    void mouseReleaseEvent(QMouseEvent *);//重写鼠标释放事件
    void mouseMoveEvent(QMouseEvent *);//重写鼠标移动事件

private:
    void pixelAt(QPainter* painter, Point& p);
    void pixelAt(QPainter*, double x, double y);
    void lineAt(QPainter*, Point from, Point to);
    void drawPolygonBorder(QPainter *, QColor& , Polygon &);
    void fillPolygon(QPainter *, QColor& , Polygon &);
    void paintPolygon(QPainter*, QColor& borderColor, QColor& fillColor, Polygon& p);

private slots:
    void slot_btnGroupClicked(int);
    void on_listWidget_clicked(const QModelIndex &index);
    void on_borderColorBtn_clicked();
    void on_fillColorBtn_clicked();
    void on_confirmBtn_clicked();
    void on_subjectComboBox_currentIndexChanged(int index);
    void on_clipComboBox_currentIndexChanged(int index);
    void on_clipBtn_clicked();
    void on_saveBtn_clicked();
};

#endif // WIDGET_H
