#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "object.h"

double distance(Point& p1, Point& p2);
bool isClockwise(vector<Point>&);
pair<bool, Point> intersect(Point& s1, Point& e1, Point& s2, Point& e2);
pair<bool, Point> intersect(Line& line1, Line& line2);
bool pointInCycle(vector<Point>& cycle, Point& p);

#endif // GEOMETRY_H
