#include "object.h"
#include <cmath>

#define PI 3.1415

Point::Point():x(0), y(0), type(VERTEX){}

Point::Point(double xx, double yy):x(xx), y(yy), type(VERTEX){}

Point::Point(double xx, double yy, char type):x(xx), y(yy), type(type){}

void Point::translate(double tx, double ty)
{
    x += tx;
    y += ty;
}

void Point::rotate(double theta)
{
    double cosine = cos(2 * PI * theta / 360);
    double sine = sin(2 * PI * theta / 360);
    double nx = x * cosine - y * sine;
    double ny = x * sine + y * cosine;
    x = nx;
    y = ny;
}

void Point::scale(double s)
{
    x = x * s;
    y = y * s;
}

bool Point::operator ==(const Point& p){
    return equal(x, p.x) && equal(y, p.y);
}

bool Point::operator !=(const Point& p){
    return !(*this == p);
}

Line::Line(Point ss, Point ee): s(ss), e(ee){}

Polygon::Polygon(): borderColor(Qt::red), fillColor(Qt::blue) {}

void Polygon::translate(double tx, double ty)
{
    size_t num = vertices.size();
    for(size_t i=0; i<num; i++){
        vertices[i].translate(tx, ty);
    }
    size_t num2 = innerVertices.size();
    for(size_t i=0; i<num2; i++){
        num = innerVertices[i].size();
        for(size_t j=0; j<num; j++){
            innerVertices[i][j].translate(tx, ty);
        }
    }
}

void Polygon::rotate(double theta)
{
    size_t num = vertices.size();
    for(size_t i=0; i<num; i++){
        vertices[i].rotate(theta);
    }
    size_t num2 = innerVertices.size();
    for(size_t i=0; i<num2; i++){
        num = innerVertices[i].size();
        for(size_t j=0; j<num; j++){
            innerVertices[i][j].rotate(theta);
        }
    }
}

void Polygon::scale(double s)
{
    size_t num = vertices.size();
    for(size_t i=0; i<num; i++){
        vertices[i].scale(s);
    }
    size_t num2 = innerVertices.size();
    for(size_t i=0; i<num2; i++){
        num = innerVertices[i].size();
        for(size_t j=0; j<num; j++){
            innerVertices[i][j].scale(s);
        }
    }
}

vector<list<Point>> Polygon::allPoints()
{
    size_t num = innerVertices.size();
    vector<list<Point>> v(1+num, list<Point>());
    for(auto it = vertices.begin(); it != vertices.end(); it++){
        v[0].push_back(*it);
    }
    for(size_t i=0; i<num; i++){
        for(auto it = innerVertices[i].begin(); it != innerVertices[i].end(); it++){
            v[i+1].push_back(*it);
        }
    }
    return v;
}

list<Line> Polygon::allEdges()
{
    list<Line> list;
    size_t vertice_num = vertices.size();
    for(size_t i=0; i<vertice_num; i++){
        list.push_back(Line(vertices[i], vertices[(i+1)%vertice_num]));
    }
    size_t num = innerVertices.size();
    for(size_t i=0; i<num; i++){
        vertice_num = innerVertices[i].size();
        for(size_t j=0; j<vertice_num; j++){
            list.push_back(Line(innerVertices[i][j], innerVertices[i][(j+1)%vertice_num]));
        }
    }
    return list;
}

bool equal(double a, double b){
    return abs(a-b) < 1e-6;
}

vector<Point> reverse(vector<Point>& v){
    size_t num = v.size();
    vector<Point> reversed(num);
    for(unsigned i=0; i<num; i++){
        reversed[i] = v[num-i-1];
    }
    return reversed;
}
