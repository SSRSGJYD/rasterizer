#include "clip.h"
#include "geometry.h"

void insertToPoints(Point& p, vector<list<Point>>& v, Point& lineStart, Point& lineEnd)
{
    size_t num = v.size();
    list<Point>::iterator start;
    size_t i = 0;
    for(; i<num; i++){
        start = find(v[i].begin(), v[i].end(), lineStart);
        if(start != v[i].end())
            break;
    }
    auto end_line = find(v[i].begin(), v[i].end(), lineEnd);
    auto it = start;
    double distToStart = distance(p, *it);
    while (it != end_line && it != v[i].end()) {
        if (distance(*it, *start) >= distToStart) {
            break;
        }
        it++;
    }
    v[i].insert(it, p);
}

pair<Point, unsigned> traverse(Point& start, list<Point>& inPoints, list<Point>& outPoints, vector<list<Point>>& polygonPoints, vector<bool>& visited, Polygon& polygon)
{
    size_t num = polygonPoints.size();
    list<Point>::iterator it;
    size_t i = 0;
    for(; i<num; i++){
        it = find(polygonPoints[i].begin(), polygonPoints[i].end(), start);
        if(it != polygonPoints[i].end())
            break;
    }
    if(i > 0){
        visited[i-1] = true;
    }
    unsigned index = 0;
    while (true) {
        polygon.vertices.push_back(*it++);
        if (it == polygonPoints[i].end()) {
            it = polygonPoints[i].begin();
        }
        // if meet a intersection point, stop
        list<Point>::iterator idx;
        for(idx = inPoints.begin(), index=0; idx != inPoints.end(); idx++, index++){
            if(*idx == *it) break;
        }
        if (idx != inPoints.end()) {
            break;
        }
        if (find(outPoints.begin(), outPoints.end(), *it) != outPoints.end()) {
            break;
        }
    }
    return pair<Point, unsigned>(*it, index); // the stopped point
}

vector<Polygon>* Weiler_Atherton(Polygon &subject, Polygon &clip)
{
    vector<Polygon> *result = new vector<Polygon>();
    if (subject.vertices.size() < 3 || clip.vertices.size() < 3) {
        return result;
    }

    vector<list<Point>> subjectPoints = subject.allPoints();
    vector<list<Point>> clipPoints = clip.allPoints();
    list<Line> subjectEdges = subject.allEdges();
    list<Line> clipEdges = clip.allEdges();

    // mark whether the inner cycle is visited
    size_t subjectOuterNum = subject.vertices.size();
    size_t clipOuterNum = clip.vertices.size();
    size_t subjectInnerNum = subject.innerVertices.size();
    size_t clipInnerNum = clip.innerVertices.size();
    vector<bool> subjectInnerVisited(subjectInnerNum, false);
    vector<bool> clipInnerVisited(clipInnerNum, false);

    // find IN and OUT points
    list<Point> inPoints;
    size_t inPointsNum = 0;
    vector<bool> inPointOnOuterCycle;
    list<Point> outPoints;
    bool intersectOnOuterCycle = false;
    size_t i = 0;
    for (auto subjectEdge : subjectEdges) {
        size_t j = 0;
        for (auto clipEdge : clipEdges) {
            auto result = intersect(subjectEdge, clipEdge);
            if (result.first) {
                // there is an intersection point
                Point p = result.second;
                if (p.type == Point::IN) {
                    inPoints.push_back(p);
                    inPointsNum++;
                    if(i < subjectOuterNum || j < clipOuterNum){
                        intersectOnOuterCycle = true;
                        inPointOnOuterCycle.push_back(true);
                    }
                    else{
                        inPointOnOuterCycle.push_back(false);
                    }
                }
                else { // OUT
                    outPoints.push_back(p);
                }
                // add intersection point to 2 point tables
                insertToPoints(p, subjectPoints, subjectEdge.s, subjectEdge.e);
                insertToPoints(p, clipPoints, clipEdge.s, clipEdge.e);
            }
            j++;
        }
        i++;
    }

    if(!intersectOnOuterCycle){
        // no intersection on any outer edge of 2 polygons
        // Condition 1: one contain the other
        // subject in clip
        if(pointInCycle(clip.vertices, subject.vertices[0])){
            Polygon p;
            p.vertices = subject.vertices;
            result->push_back(p);
        }
        // clip in subject
        else if(pointInCycle(subject.vertices, clip.vertices[0])){
            Polygon p;
            p.vertices = clip.vertices;
            result->push_back(p);
        }
        // Conditional 2: 2 polygons are seperated
        else
            return result;
    }

    // mark whether visited in inPoints
    vector<bool> visited(inPointsNum, false);

    list<Polygon> innerCycles;
    // start traverse
    auto it = inPoints.begin();
    for(size_t i = 0; i < inPointsNum; i++, it++) {
        if(visited[i]) continue;
        visited[i] = true;
        bool isOuterCycle = false;
        Polygon currentPolygon; // a new polygon
        Point start = *it;
        Point next = start;
        pair<Point, unsigned> res;
        if(inPointOnOuterCycle[i]) isOuterCycle = true;
        do {
            res = traverse(next, inPoints, outPoints, subjectPoints, subjectInnerVisited, currentPolygon);
            next = res.first; // OUT
            res = traverse(next, inPoints, outPoints, clipPoints, clipInnerVisited, currentPolygon);
            next = res.first; // IN
            visited[res.second] = true;
            if(inPointOnOuterCycle[res.second]) isOuterCycle = true;
        } while (next != start);
        if(isOuterCycle) // a new polygon
            result->push_back(currentPolygon);
        else{
            // a new inner cycle
            innerCycles.push_back(currentPolygon);
        }
    }

    size_t result_num = result->size(); // number of polygons (outer cycles)

    // for each inner cycle, find corresponding outer cycle
    for(auto it=innerCycles.begin(); it != innerCycles.end(); it++){
        for(size_t j=0; j<result_num; j++){
            if(pointInCycle((*result)[j].vertices, it->vertices[0])){
                // find the correct polygon
                (*result)[j].innerVertices.push_back(it->vertices);
                break;
            }
        }
    }

    // deal with inner cycles that are not visited
    for(size_t i=0; i<subjectInnerNum; i++){
        if(subjectInnerVisited[i]) continue;
        for(size_t j=0; j<result_num; j++){
            if(pointInCycle((*result)[j].vertices, subject.innerVertices[i][0])){
                // add inner cycle to the polygon
                (*result)[j].innerVertices.push_back(subject.innerVertices[i]);
                break;
            }
        }
    }
    for(size_t i=0; i<clipInnerNum; i++){
        if(clipInnerVisited[i]) continue;
        for(size_t j=0; j<result_num; j++){
            if(pointInCycle((*result)[j].vertices, clip.innerVertices[i][0])){
                // add inner cycle to the polygon
                (*result)[j].innerVertices.push_back(clip.innerVertices[i]);
                break;
            }
        }
    }

    return result;
}




