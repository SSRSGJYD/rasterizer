#include "geometry.h"
#include <cmath>

double distance(Point& p1, Point& p2){
    return sqrt(pow(p1.x-p2.x, 2) + pow(p1.y-p2.y, 2));
}

bool isClockwise(vector<Point> &v)
{
    // find a convex vertex
    double max_x = v[0].x;
    unsigned max_x_idx = 0;
    size_t num = v.size();
    for(unsigned i=1; i<num; i++){
        if(v[i].x > max_x){
            max_x = v[i].x;
            max_x_idx = i;
        }
    }
    // cross product
    unsigned prev = max_x_idx == 0 ? num-1 : max_x_idx-1;
    unsigned next = max_x_idx == num-1 ? 0 : max_x_idx+1;
    double x1 = v[max_x_idx].x - v[prev].x;
    double y1 = v[max_x_idx].y - v[prev].y;
    double x2 = v[next].x - v[max_x_idx].x;
    double y2 = v[next].y - v[max_x_idx].y;
    double cross_product = x2 * y1 - x1 * y2;
    return cross_product > 0;
}

pair<bool, Point> intersect(Point &s1, Point &e1, Point &s2, Point &e2)
{
    double a1 = e1.y - s1.y;
    double b1 = s1.x - e1.x;
    double c1 = a1 * s1.x + b1 * s1.y;
    double a2 = e2.y - s2.y;
    double b2 = s2.x - e2.x;
    double c2 = a2 * s2.x + b2 * s2.y;

    // cross product
    double determinant = a1 * b2 - a2 * b1;

    // parallel
    if (equal(determinant, 0)) {
        return pair<bool, Point>(false, Point());
    }
    else {
        // intersection point
        double x = (b2 * c1 - b1 * c2) / determinant;
        double y = (a1 * c2 - a2 * c1) / determinant;

        // check if point belongs to segment
        if (x < min(s1.x, e1.x) || x > max(s1.x, e1.x))
            return pair<bool, Point>(false, Point());
        if (y < min(s1.y, e1.y) || y > max(s1.y, e1.y))
            return pair<bool, Point>(false, Point());
        if (x < min(s2.x, e2.x) || x > max(s2.x, e2.x))
            return pair<bool, Point>(false, Point());
        if (y < min(s2.y, e2.y) || y > max(s2.y, e2.y))
            return pair<bool, Point>(false, Point());

        // judge IN and OUT by cross product
        return pair<bool, Point>(true, Point(x, y, determinant < 0 ? Point::IN : Point::OUT));
    }
}

pair<bool, Point> intersect(Line& line1, Line& line2){
    return intersect(line1.s, line1.e, line2.s, line2.e);
}

bool pointInCycle(vector<Point>& cycle, Point& p) {

    size_t num= cycle.size();
    size_t i,j = num-1;
    bool odd = false;

    for (i = 0; i < num; i++) {
        if(((cycle[i].y < p.y && cycle[j].y >= p.y) || (cycle[j].y < p.y && cycle[i].y >= p.y))
            && (cycle[i].x <= p.x || cycle[j].x <= p.x)) {
            odd ^= (cycle[i].x + (p.y-cycle[i].y)/(cycle[j].y-cycle[i].y)*(cycle[j].x-cycle[i].x) < p.x);
        }
        j=i;
    }
    return odd;
}
