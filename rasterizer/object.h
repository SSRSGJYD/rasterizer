#ifndef OBJECT_H
#define OBJECT_H

#include <vector>
#include <QColor>
using namespace std;

class Point {
public:
    enum {VERTEX, IN, OUT};
    double x;
    double y;
    char type; // VERTEX, IN, OUT
public:
    Point();
    Point(double xx, double yy);
    Point(double xx, double yy, char type);
    void translate(double tx, double ty);
    void rotate(double theta);
    void scale(double s);
    bool operator ==(const Point& p);
    bool operator !=(const Point& p);
};

class Line {
public:
    Point s;
    Point e;
public:
    Line(Point ss, Point ee);

};

class Polygon {
public:
    vector<Point> vertices;
    vector<vector<Point>> innerVertices;
    QColor borderColor;
    QColor fillColor;

public:
    Polygon();
    void translate(double tx, double ty);
    void rotate(double theta);
    void scale(double s);
    vector<list<Point>> allPoints();
    list<Line> allEdges();
};

bool equal(double a, double b);
vector<Point> reverse(vector<Point>&);

#endif // OBJECT_H
