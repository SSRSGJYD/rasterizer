#include "widget.h"
#include "ui_widget.h"
#include "clip.h"
#include "geometry.h"
#include "rasterize.h"

#include <cmath>
#include <QColorDialog>
#include <QComboBox>
#include <QDebug>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget), newPolygon(nullptr), newInnerRing(nullptr),
    borderColor(Qt::red), fillColor(Qt::blue),
    trackingMouse(false),
    subjectIndex(0), clipIndex(0), clipPolygons(nullptr)
{
    ui->setupUi(this);
    // color label
    QPalette pal1(ui->borderColorLabel->palette());
    pal1.setColor(QPalette::Background, borderColor);
    ui->borderColorLabel->setAutoFillBackground(true);
    ui->borderColorLabel->setPalette(pal1);
    QPalette pal2(ui->fillColorLabel->palette());
    pal2.setColor(QPalette::Background, fillColor);
    ui->fillColorLabel->setAutoFillBackground(true);
    ui->fillColorLabel->setPalette(pal2);

    // button group
    btnGroup = new QButtonGroup(this);
    btnGroup->addButton(ui->viewRadioBtn, VIEW);
    btnGroup->addButton(ui->outerCycleRadioBtn, OUTER_RING);
    btnGroup->addButton(ui->innerCycleRadioBtn, INNER_RING);
    btnGroup->addButton(ui->translateRadioBtn, TRANSLATE);
    btnGroup->addButton(ui->rotateRadioBtn, ROTATE);
    btnGroup->addButton(ui->scaleRadioBtn, SCALE);
    btnGroup->addButton(ui->clipRadioBtn, CLIP);
    ui->viewRadioBtn->setChecked(true); // by default
    ui->rotateEdit->setEnabled(false);
    ui->scaleEdit->setEnabled(false);
    ui->subjectComboBox->addItem(QString("None"));
    ui->subjectComboBox->setEnabled(false);
    ui->clipComboBox->addItem(QString("None"));
    ui->clipComboBox->setEnabled(false);
    ui->clipBtn->setEnabled(false);
    ui->saveBtn->setEnabled(false);
    connect(btnGroup, SIGNAL(buttonClicked(int)), this, SLOT(slot_btnGroupClicked(int)));

    // confirm button
    ui->confirmBtn->setEnabled(false);

    // regular expression to limit content in line edit
    QRegExp signed_real("^(-?[0-9]+)(.[0-9]+)?$");
    QRegExp unsigned_real("^[1-9][0-9]*.[0-9]+|0.[0-9]*[1-9]$");
    ui->rotateEdit->setValidator(new QRegExpValidator(signed_real, this));
    ui->scaleEdit->setValidator(new QRegExpValidator(unsigned_real, this));

    // mouse tracking
    setMouseTracking(true);
    ui->frame->setMouseTracking(true);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::slot_btnGroupClicked(int id) {
    mode = id;
    // enable and disable line edit
    if(mode == ROTATE){
        ui->rotateEdit->setEnabled(true);
        ui->scaleEdit->setEnabled(false);
        ui->confirmBtn->setEnabled(true);
    }
    else if(mode == SCALE){
        ui->rotateEdit->setEnabled(false);
        ui->scaleEdit->setEnabled(true);
        ui->confirmBtn->setEnabled(true);
    }
    else {
        ui->rotateEdit->setEnabled(false);
        ui->scaleEdit->setEnabled(false);
        ui->confirmBtn->setEnabled(false);
    }
    // enable and disable combo box and clip btn
    if(mode == CLIP) {
        ui->subjectComboBox->setCurrentIndex(0); // None
        ui->subjectComboBox->setEnabled(true);
        ui->clipComboBox->setCurrentIndex(0); // None
        ui->clipComboBox->setEnabled(true);
        ui->clipBtn->setEnabled(true);
    }
    else {
        ui->subjectComboBox->setEnabled(false);
        ui->clipComboBox->setEnabled(false);
        ui->clipBtn->setEnabled(false);
        ui->saveBtn->setEnabled(false);
        // clear previous clipping result
        delete clipPolygons;
        clipPolygons = nullptr;
    }
    // clear newPolygon
    if(mode != OUTER_RING && newPolygon != nullptr) {
        delete newPolygon;
        newPolygon = nullptr;
    }
    // clear newInnerRing
    if(mode != INNER_RING && newInnerRing != nullptr) {
        delete newInnerRing;
        newInnerRing = nullptr;
    }
    // enable and disable color dialog
    if(mode == VIEW){
        ui->borderColorBtn->setEnabled(true);
        ui->fillColorBtn->setEnabled(true);
    }
    else{
        ui->borderColorBtn->setEnabled(false);
        ui->fillColorBtn->setEnabled(false);
    }
    // clear old polygons
    update();
}

void Widget::paintEvent(QPaintEvent *) {
    windowx = ui->frame->x();
    windowy = ui->frame->y();

    // painter
    QPainter painter(this);
    painter.setClipRect(windowx, windowy, 2*windoww, 2*windowh);
    painter.setRenderHint(QPainter:: Antialiasing, true);

    switch (mode) {
    case VIEW:
    case ROTATE:
    case SCALE:
    case INNER_RING: {
        int idx = ui->listWidget->currentRow();
        if(idx >= 0)
            paintPolygon(&painter, borderColor, fillColor, polygons[size_t(idx)]);
        if(mode == INNER_RING) {
            if(newInnerRing != nullptr){
                // paint currently added inner ring
                size_t num = newInnerRing->size();
                painter.setPen(QPen(borderColor));
                for(size_t i=0; i<num-1; i++){
                    lineAt(&painter, (*newInnerRing)[i], (*newInnerRing)[i+1]);
                }
                // show new edge to be added
                lineAt(&painter, lastPoint, newPoint);
            }
        }
        break;
    }
    case OUTER_RING: {
        if(newPolygon != nullptr){
            size_t num = newPolygon->vertices.size();
            painter.setPen(QPen(borderColor));
            for(size_t i=0; i<num-1; i++){
                lineAt(&painter, newPolygon->vertices[i], newPolygon->vertices[i+1]);
            }
            // show new edge to be added
            lineAt(&painter, lastPoint, newPoint);
        }
        break;
    }
    case TRANSLATE: {
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            paintPolygon(&painter, borderColor, fillColor, polygons[size_t(idx)]);
        }
        break;
    }
    case CLIP:{
        if(subjectIndex > 0){
            drawPolygonBorder(&painter, polygons[size_t(subjectIndex-1)].borderColor, polygons[size_t(subjectIndex-1)]);
        }
        if(clipIndex > 0){
            drawPolygonBorder(&painter, polygons[size_t(clipIndex-1)].borderColor, polygons[size_t(clipIndex-1)]);
        }
        // show cliping result
        if(clipPolygons != nullptr){
            size_t num = clipPolygons->size();
            for(size_t i=0; i<num; i++){
                fillPolygon(&painter, polygons[size_t(subjectIndex-1)].fillColor, (*clipPolygons)[i]);
            }
        }
        break;
    }
    default:
        break;
    }

}

void Widget::mousePressEvent(QMouseEvent *e)
{
    if(mode == OUTER_RING) {
        if(e->button() == Qt::LeftButton) {
            // a new vertice
            if(newPolygon == nullptr)
                newPolygon = new Polygon();
            int x = e->pos().x()-windowx-windoww;
            int y = windowy+windowh-e->pos().y();
            newPolygon->vertices.push_back(Point(x, y));
            newPoint = lastPoint = Point(x, y);
            update();
        }
        else if(e->button() == Qt::RightButton) {
            // finish
            if(newPolygon == nullptr) return;
            // check if outer ring is counterclockwise
            if(isClockwise(newPolygon->vertices))
                newPolygon->vertices = reverse(newPolygon->vertices);
            polygons.push_back(*newPolygon);
            delete newPolygon;
            newPolygon = nullptr;
            ui->viewRadioBtn->setChecked(true);
            mode = VIEW;
            ui->listWidget->addItem(QString("Polygon %1").arg(polygons.size()));
            ui->listWidget->setCurrentRow(int(polygons.size()));
            ui->subjectComboBox->addItem(QString("Polygon %1").arg(polygons.size()));
            ui->clipComboBox->addItem(QString("Polygon %1").arg(polygons.size()));
            update();
        }
    }
    else if(mode == INNER_RING) {
        if(e->button() == Qt::LeftButton) {
            int idx = ui->listWidget->currentRow();
            if(idx >= 0){
                // a new inner ring vertex
                int x = e->pos().x()-windowx-windoww;
                int y = windowy+windowh-e->pos().y();
                if(newInnerRing == nullptr)
                    newInnerRing = new vector<Point>();
                newInnerRing->push_back(Point(x, y));
                newPoint = lastPoint = Point(x, y);
                update();
            }
        }
        else if(e->button() == Qt::RightButton) {
            // finish
            if(newInnerRing == nullptr) return;
            int idx = ui->listWidget->currentRow();
            if(idx >= 0){
                // check if inner ring is clockwise
                if(!isClockwise(*newInnerRing))
                    *newInnerRing = reverse(*newInnerRing);
                polygons[size_t(idx)].innerVertices.push_back(*newInnerRing);
                delete newInnerRing;
                newInnerRing = nullptr;
                update();
            }
        }
    }
    else if(mode == TRANSLATE && e->button() == Qt::LeftButton){
        trackingMouse = true;
        // change shape of cursor
        ui->frame->setCursor(Qt::OpenHandCursor);
        translateStartX = e->pos().x() - windowx - windoww;
        translateStartY = windowy + windowh - e->pos().y();
    }
}

void Widget::mouseReleaseEvent(QMouseEvent *e)
{
    if(mode == TRANSLATE && trackingMouse && e->button() == Qt::LeftButton){
        trackingMouse = false;
        // change shape of cursor
        ui->frame->setCursor(Qt::CrossCursor);
    }
}

void Widget::mouseMoveEvent(QMouseEvent *e)
{
    int x = e->pos().x() - windowx - windoww;
    int y = windowy + windowh - e->pos().y();
    // show position of mouse
    if(x >= -windoww && x <= windoww && y >= -windowh && y <= windowh) {
        ui->cursorLabel->setText(QString("Cursor:(%1, %2)").arg(x).arg(y));
        if(mode == OUTER_RING){
            if(newPolygon != nullptr){
                newPoint = Point(x, y);
                update();
            }
        }
        else if(mode == INNER_RING){
            if(newInnerRing != nullptr){
                newPoint = Point(x, y);
                update();
            }
        }
    }
    // real time translation
    if(mode == TRANSLATE && trackingMouse){
        translateEndX = e->pos().x() - windowx - windoww;
        translateEndY = windowy + windowh - e->pos().y();
        int tx = translateEndX - translateStartX;
        int ty = translateEndY - translateStartY;
        translateStartX = translateEndX;
        translateStartY = translateEndY;
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            polygons[size_t(idx)].translate(tx, ty);
        }
        update();
    }
}

void Widget::pixelAt(QPainter* painter, Point& p) {
    painter->drawPoint(int(p.x)+windowx+windoww, windowy+windowh-int(p.y));
}

void Widget::pixelAt(QPainter* painter, double x, double y) {
    painter->drawPoint(int(x)+windowx+windoww, windowy+windowh-int(y));
}

void Widget::lineAt(QPainter* painter, Point from, Point to)
{
    painter->drawLine(int(floor(from.x))+windowx+windoww, windowy+windowh-int(from.y),
                      int(ceil(to.x))+windowx+windoww, windowy+windowh-int(to.y));
}

void Widget::drawPolygonBorder(QPainter *painter, QColor& color, Polygon &p){
    size_t num = p.vertices.size();
    // paint outer border
    painter->setPen(QPen(color));
    for(size_t i=0; i<num-1; i++){
        lineAt(painter, p.vertices[i], p.vertices[i+1]);
    }
    lineAt(painter, p.vertices[num-1], p.vertices[0]);
    // paint inner ring
    num = p.innerVertices.size();
    for(size_t i=0; i<num; i++){
        size_t num2 = p.innerVertices[i].size();
        // paint outer border
        for(size_t j=0; j<num2-1; j++){
            lineAt(painter, p.innerVertices[i][j], p.innerVertices[i][j+1]);
        }
        lineAt(painter, p.innerVertices[i][num2-1], p.innerVertices[i][0]);
    }
}

void Widget::fillPolygon(QPainter *painter, QColor& color, Polygon &p){
    // paint fill area
    painter->setPen(QPen(color));
    queue<pair<Point, Point> > fillLines = rasterize(p);
    while(!fillLines.empty()){
        pair<Point, Point> pa = fillLines.front();
        fillLines.pop();
        lineAt(painter, pa.first, pa.second);
    }
}


void Widget::paintPolygon(QPainter *painter, QColor& borderColor, QColor& fillColor, Polygon &p)
{
    fillPolygon(painter, fillColor, p);
    drawPolygonBorder(painter, borderColor, p);
}

void Widget::on_listWidget_clicked(const QModelIndex &index)
{
    ui->viewRadioBtn->setChecked(true);
    // change to item color
    int idx = index.row();
    if(idx >= 0){
        borderColor = polygons[size_t(idx)].borderColor;
        QPalette pal1(ui->borderColorLabel->palette());
        pal1.setColor(QPalette::Background, borderColor);
        ui->borderColorLabel->setPalette(pal1);

        fillColor = polygons[size_t(idx)].fillColor;
        QPalette pal2(ui->fillColorLabel->palette());
        pal2.setColor(QPalette::Background, fillColor);
        ui->fillColorLabel->setPalette(pal2);

        emit btnGroup->buttonClicked(VIEW);
    }

}

void Widget::on_borderColorBtn_clicked()
{
    if(mode == VIEW){
        QColorDialog dialog;
        borderColor = dialog.getRgba();
        QPalette pal1(ui->borderColorLabel->palette());
        pal1.setColor(QPalette::Background, borderColor);
        ui->borderColorLabel->setPalette(pal1);
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            polygons[size_t(idx)].borderColor = borderColor;
            update();
        }
    }
}

void Widget::on_fillColorBtn_clicked()
{
    if(mode == VIEW){
        QColorDialog dialog;
        fillColor = dialog.getRgba();
        QPalette pal2(ui->fillColorLabel->palette());
        pal2.setColor(QPalette::Background, fillColor);
        ui->fillColorLabel->setPalette(pal2);
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            polygons[size_t(idx)].fillColor = fillColor;
            update();
        }
    }
}

void Widget::on_confirmBtn_clicked()
{
    if(mode == ROTATE){
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            double theta = ui->rotateEdit->text().toDouble();
            polygons[size_t(idx)].rotate(theta);
            update();
        }
    }
    else if(mode == SCALE){
        int idx = ui->listWidget->currentRow();
        if(idx >= 0){
            double s = ui->scaleEdit->text().toDouble();
            polygons[size_t(idx)].scale(s);
            update();
        }
    }
}

void Widget::on_subjectComboBox_currentIndexChanged(int index)
{
    subjectIndex = index;
    delete clipPolygons;
    clipPolygons = nullptr;
    update();
}

void Widget::on_clipComboBox_currentIndexChanged(int index)
{
    clipIndex = index;
    delete clipPolygons;
    clipPolygons = nullptr;
    update();
}

void Widget::on_clipBtn_clicked()
{
    if(subjectIndex == 0 || clipIndex == 0){
        // haven't choose two polygons yet
        QMessageBox::warning(this, QString("Error"),
            QString("You should choose subject and clip polygons first!"));
    }
    else{
        if(clipPolygons == nullptr){
            // no clip calculation yet
            clipPolygons = Weiler_Atherton(polygons[size_t(subjectIndex-1)],
                                            polygons[size_t(clipIndex-1)]);
            ui->saveBtn->setEnabled(true);
            update();
        }
    }
}

void Widget::on_saveBtn_clicked()
{
    if(clipPolygons == nullptr || clipPolygons->size() == 0){
        QMessageBox::warning(this, QString("Error"),
            QString("Nothing to save!"));
    }
    size_t num = clipPolygons->size();
    for(size_t i=0; i<num; i++){
        polygons.push_back((*clipPolygons)[i]);
        ui->listWidget->addItem(QString("Polygon %1").arg(polygons.size()));
        ui->listWidget->setCurrentRow(int(polygons.size()));
        ui->subjectComboBox->addItem(QString("Polygon %1").arg(polygons.size()));
        ui->clipComboBox->addItem(QString("Polygon %1").arg(polygons.size()));
    }
}
